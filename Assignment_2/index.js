function validate()
{   
    if(first_name()==false)
    {
    return false;
    }
    if(last_name()==false)
    {
    return false;
    }
    if(phone_no()===false)
    {
    return false;
    }
    if(office_no()==false)  
    {
    return false;
    }
    if(email()==false)
    {
    return false;
    }
    if(pass1()==false)    
    {
    return false;
    }
    if(conpass()==false)
    {
    return false;
    }
    if(validate_age()==false)
    {
    return false;
    }
    if(cb()==false)
    {
    return false;
    }
    if(abts()==false)
    {
    return false;
    }

}
$(document).ready(function()
{
    function first_name()
    {
    var fname = $("#text1").val();
    var regx = /^[a-zA-Z]+$/;

    if(regx.test(fname)=="")
    {
        $("#wrong").css("visibility","visible");
        $("#correct").css("visibility","hidden");
        return false;
    }
    
    else
    {
        $("#correct").css("visibility","visible");
        $("#wrong").css("visibility","hidden");
    }
    }

    function last_name()
    {
        var lname = $("#text2").val();
        var regx = /^[a-zA-Z]+$/;

        if(regx.test(lname)=="")
        {
            $("#correct1").css("visibility","hidden");
            $("#wrong1").css("visibility","visible");
            return false;
        }
        else
        {
            $("#correct1").css("visibility","visible");
            $("#wrong1").css("visibility","hidden");
        }
    }

    function phone_no()
    {
        var phone = $("#text3").val();
        var regx = /^[6-9]\d{9}$/

        if(regx.test(phone)=="")
        {
            $("#correct2").css("visibility","hidden");
            $("#wrong2").css("visibility","visible");
            return false; 
        }
        else
        {
            $("#correct2").css("visibility","visible");
            $("#wrong2").css("visibility","hidden");
        }
    }

    function office_no()
    {
        var off = $("#text4").val();
        var regx = /^[0-9]{4}[0-9]{7}$/;

        if(regx.test(off)=="")
        {
            $("#correct3").css("visibility","hidden");
            $("#wrong3").css("visibility","visible");
            return false; 
        }
        else
        {
            $("#correct3").css("visibility","visible");
            $("#wrong3").css("visibility","hidden");
        }
    }
    
    function email()
    {
        var mail = $("#text5").val();
        var regx = /[\w-]+@([\w-]+\.)+[\w-]+/

        if(regx.test(mail)=="")
        {
            $("#correct4").css("visibility","hidden");
            $("#wrong4").css("visibility","visible");
            return false;
        }
        else
        {
            $("#correct4").css("visibility","visible");
            $("#wrong4").css("visibility","hidden");
            
        }
    }
    var pas;
    function pass1()
    {
        var pas = $("#text6").val();    
        var regx = /^(?=.*[a-zA-Z])(?=.*[0-9])/

        if(regx.test(pas)=="")
        {
            $("#correct5").css("visibility","hidden");
            $("#wrong5").css("visibility","visible");
            return false;
        }
        else
        {
            $("#correct5").css("visibility","visible");
            $("#wrong5").css("visibility","hidden");
               
        }
    }
    
    function conpass()
    {
        var pas1 = $("#text7").val();
        
        if(pas==pas1)
        {
            $("#correct6").css("visibility","hidden");
            $("#wrong6").css("visibility","visible");
            return false;
        }
        else
        {
            $("#correct6").css("visibility","visible");
            $("#wrong6").css("visibility","hidden");  
        }
    }
    
    function validate_age()
{
    var date=new Date;
    var m=date.getMonth()+1;
    var y=date.getFullYear()-1;
    var q=birthMonth-m;
    var birthDay = $("#day").val();
    var birthMonth = $("#month").val();
    var birthYear = $("#year").val();
        //var age = $("#age").val();

    if(birthMonth=="month"||birthYear=="year"||birthDay=="day")
    {
    $("#correct10").css("visibility","hidden");
    $("#wrong10").css("visibility","visible");
    $("#correct10").css("visibility","hidden");
    $("#wrong10").css("visibility","visible");
    return false;
    }

    if(birthMonth!="month" && birthYear!="year" && birthDay!="day")
    {
    var date=new Date;
    var m=date.getMonth()+1;
    var y=date.getFullYear()-1;
    var q=birthMonth-m;
    var age=(y-birthYear)+(Math.abs(12-q)/10)
    $("#age").val(age)
    //$("#age").disabled= true;
    if(age!="")
    {
        $("#correct10").css("visibility","visible");
        $("#wrong10").css("visibility","hidden");
        $("#correct10").css("visibility","visible");
        $("#wrong10").css("visibility","hidden");

    }
    }
}

    function cb()
    {
        

    
        if ($("#checkbox_sample18").is(":checked")==false && $("#checkbox_sample19").is(":checked")==false && $("#checkbox_sample20").is(":checked")==false)
        {
            $("#correct11").css("visibility","hidden");
            $("#wrong11").css("visibility","visible");
            return false;
        }
        else
        {
            $("#correct11").css("visibility","visible");
            $("#wrong11").css("visibility","hidden");  
        }
    }

    function abts()
    {
        var abt=$("#txt").val();
        if(abt=="")
        {
            $("#correct12").css("visibility","hidden");
            $("#wrong12").css("visibility","visible");
            return false;
        }
        else
        {
            $("#correct12").css("visibility","visible");
            $("#wrong12").css("visibility","hidden");  
        }
    }
    $("#text1").blur(first_name);
    $("#text2").blur(last_name);
    $("#text3").blur(phone_no);
    $("#text4").blur(office_no);
    $("#text5").blur(email);
    $("#text6").blur(pass1);
    $("#text7").blur(conpass);
    $("#day,#month,#year").change(validate_age);
    $("#txt").blur(abts);
    $("#submt").click(validate);
});
function yearCalculation(){
    var selectBox = document.getElementById('year');
    var s = new Date();
    second = s.getFullYear();
    
    for (var i = second; i >= 1950; i--) {
    
    var option = document.createElement('option');
    
    option.value = i;
    option.innerHTML = i;
    
    selectBox.appendChild(option);
    }
    }





